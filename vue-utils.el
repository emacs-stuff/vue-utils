;; vindarel 2017

(defun vue-utils-goto-script ()
  "In a vue file, go to the <script> part."
  (interactive)
  (beginning-of-buffer)
  (search-forward "<script>" nil t)
  (next-line)
  (beginning-of-line-text))

(defun vue-utils-goto-template ()
  "In a vue file, go to the <template> part."
  (interactive)
  (beginning-of-buffer)
  (search-forward "<template>" nil t)
  (next-line)
  (beginning-of-line-text))

(defhydra vue-utils-hydra (:color red)
  "
  Vue
  "
  ("t" vue-utils-goto-template "go to template")
  ("s" vue-utils-goto-script "go to script")
  ("i" helm-imenu "imenu")) ;; only shows the template's headers, not JS.
