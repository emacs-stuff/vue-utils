;; vindarel 2017

;; Vue helpers:
;; - migration helpers

(defun vue-utils-migrate-value-vbind ()
  "Replace value=\"{{ foo }}\") with v-bind:value=\"foo\"."
  (interactive)
  ;;  [^:] = don't pick v-bind:match
  (query-replace-regexp "[^:]\\(value\\|selected\\)=.*{{ ?\\(.*\\) }}\"" "v-bind:\\1=\"\\2\""))

(defun vue-utils-migrate-interpolation-within-attributes ()
  "  Reason: Interpolation within attributes has been removed
  More info: http://vuejs.org/guide/migration.html#Interpolation-within-Attributes
"
  (interactive)
  ;; name="field-{{ field.name }}" to v-bind:name="'field-' + field.name"
  (query-replace-regexp " \\(name\\|id\\)=\"field-{{ ?\\(.*\\) }}\"" " v-bind:\\1=\"'field-' + \\2\"")
  (query-replace-regexp "[^:]\\(id\\|for\\|name\\|readonly\\|selected\\)=.*{{ ?\\(.*\\) }}\"" " v-bind:\\1=\"\\2\""))
  (query-replace-regexp "[^:]\\(id\\|for\\|name\\|readonly\\|selected\\)=\"{{ ?\\([A-Za-z.]*\\) }}\"" " v-bind:\\1=\"\\2\""))
  (query-replace-regexp "[^:]\\(id\\|for\\|name\\|readonly\\|selected\\)=\"{{ ?\\([A-Za-z._]*\\) }}\"" " v-bind:\\1=\"\\2\""))

(defun vue-utils-migrate-html-interpolation ()
  "  Reason: HTML Interpolation with {{{ }}} attributes has been removed
  More info: http://vuejs.org/guide/migration.html#HTML-Interpolation
"
  (interactive)
  (query-replace-regexp "{{{ ?\\(.*\\) }}}" "<div v-html=\"\\1\"></div>"))
